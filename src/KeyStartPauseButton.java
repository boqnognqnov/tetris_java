import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

public class KeyStartPauseButton implements ActionListener {
	private JFrame frame;
	private Start start;
	private InfoAndButtonsPanel controls;

	public KeyStartPauseButton(JFrame frame, Start start, InfoAndButtonsPanel controls) {
		this.frame = frame;
		this.start = start;
		this.controls = controls;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.frame.requestFocusInWindow();
		start.changeStartresumeButtonValue();

	}

}
