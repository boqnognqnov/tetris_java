import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

public class ResponsiveParams {

	public int[] getCurrentResolution() {
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		int width = gd.getDisplayMode().getWidth();
		int height = gd.getDisplayMode().getHeight();
		int[] resParams = new int[2];
		resParams[0] = width;
		resParams[1] = height;
		return resParams;

	}
}
