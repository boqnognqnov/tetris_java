import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class KeyAboutButtonEvent implements ActionListener {
	private JFrame frame;

	public KeyAboutButtonEvent(JFrame frame) {
		this.frame = frame;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.frame.requestFocusInWindow();

		JEditorPane jep = new JEditorPane();
		jep.setEditable(false);

		URL aboutPage = ClassLoader
				.getSystemResource("resources/webContents/about.html");

		try {
			jep.setPage(aboutPage);
		} catch (IOException e1) {
			jep.setContentType("text/html");
			jep.setText("<html>Could not load http://www.oreilly.com </html>");
		}

		JPanel content = new JPanel();
		content.add(jep);
		// JScrollPane content = new JScrollPane(jep);

		JFrame aboutFrame = new JFrame();
		aboutFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		aboutFrame.getContentPane().add(content);

		aboutFrame.setSize(400, 500);
		aboutFrame.setResizable(false);

		aboutFrame.setVisible(true);

	}
}