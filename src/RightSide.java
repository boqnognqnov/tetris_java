import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class RightSide extends JPanel {

	private static final long serialVersionUID = 1L;

	private FigureGenerator nextFigure = null;
	private NextPieceDrawingClass nextPieceDrawClass = null;

	private InfoAndButtonsPanel rightControls;

	// private FrameHolder frameHolder;

	public RightSide(FigureGenerator nextFigure, JFrame frame, Start start) {

		// this.frameHolder = frameHolder;

		this.setLayout(new BorderLayout());
		this.nextFigure = nextFigure;
		this.initFirstNextFigureDrawClass(frame, start);

	}

	private void initFirstNextFigureDrawClass(JFrame frame, Start start) {
		boolean NEXT_PIECE_BITS[][] = nextFigure.copy();
		nextPieceDrawClass = new NextPieceDrawingClass(NEXT_PIECE_BITS, nextFigure.getRandInt());

		Dimension dim = new Dimension(StaticData.GET_SQUARE_SIZE() * 5, StaticData.GET_SQUARE_SIZE() * 5);
		// Dimension dim = new Dimension(500, 500);
		nextPieceDrawClass.setPreferredSize(dim);

		this.add(nextPieceDrawClass, BorderLayout.NORTH);

		rightControls = new InfoAndButtonsPanel(frame, start);
		this.add(rightControls, BorderLayout.CENTER);
	}

	public void setNextFigure(FigureGenerator nextFigure) {
		this.nextFigure = nextFigure;
	}

	public void drawNextFigire() {
		boolean NEXT_PIECE_BITS[][] = nextFigure.copy();
		nextPieceDrawClass.setNEXT_PIECE_BITS(NEXT_PIECE_BITS);
		nextPieceDrawClass.setFigureType(nextFigure.getRandInt());
	}

	public void setLeftSideObj(LeftSide leftSide) {

		rightControls.setLeftSideobj(leftSide);
		rightControls.refreshInfo();

	}

}
