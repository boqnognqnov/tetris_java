import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class NextPieceDrawingClass extends JPanel {

	private static final long serialVersionUID = 1L;

	private boolean NEXT_PIECE_BITS[][] = null;
	private int figureType;

	private Image gamePanelImage;

	public void setFigureType(int figureType) {
		this.figureType = figureType;
	}

	public void setNEXT_PIECE_BITS(boolean[][] nEXT_PIECE_BITS) {
		NEXT_PIECE_BITS = nEXT_PIECE_BITS;
	}

	public NextPieceDrawingClass(boolean[][] nEXT_PIECE_BITS, int figureType) {
		this.NEXT_PIECE_BITS = nEXT_PIECE_BITS;
		this.figureType = figureType;

		BufferedInputStream stream = null;
		try {
			int[] frameSize = StaticData.GET_FRAME_SIZE();
			stream = (BufferedInputStream) getClass().getClassLoader()
					.getResourceAsStream(StaticData.PATH_NEXT_PIECE_BACKGROUND);
			BufferedImage bg = ImageIO.read(stream);
			gamePanelImage = bg.getScaledInstance(frameSize[0], frameSize[1], Image.SCALE_SMOOTH);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(gamePanelImage, 0, 0, this);

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (NEXT_PIECE_BITS[i][j]) {
					int xx = 25 + j * StaticData.GET_SQUARE_SIZE();
					int yy = 10 + i * StaticData.GET_SQUARE_SIZE();
					g.setColor(Color.decode(StaticData.colors[this.figureType]));
					g.fill3DRect(xx, yy, StaticData.GET_SQUARE_SIZE(), StaticData.GET_SQUARE_SIZE(), true);

				}

			}
		}

	}

}
