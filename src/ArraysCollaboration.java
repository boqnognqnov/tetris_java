public class ArraysCollaboration {

  private int figyreType;

  private int points = 0;

  private int leftEmptyColumns;
  private int rightEmptyColumns;

  // IS NOT USED
  private int upEmptyRows;
  // IS NOT USED
  private int downEmptyRows;

  private int leftEmptyColRotate;
  private int rightEmptyColRotate;

  // IS NOT USED
  private int upEmptyRowsRotate;
  // IS NOT USED
  private int downEmptyRowsRotate;

  private boolean[][] theBigArray = null;
  private boolean[][] figureArr = null;

  private int[][] theBigColorArr = null;

  public int getPoints() {
    return points;
  }

  public void setFigyreType(int figyreType) {
    this.figyreType = figyreType;
  }

  public void setFigureArr(boolean[][] figureArr) {
    this.figureArr = figureArr;
  }

  public int[][] getTheBigColorArray() {
    // RETURN THE BIG ARRAY TO GRAPHICS COMPONENT
    return this.theBigColorArr;
  }

  public ArraysCollaboration() {

    // GENERATE THE BIG EMPTY ARRAYS
    this.theBigArray = new boolean[StaticData.TABLE_HEIGHT][StaticData.TABLE_WIDTH];
    this.theBigColorArr = new int[StaticData.TABLE_HEIGHT][StaticData.TABLE_WIDTH];

    for (int i = 0; i < StaticData.TABLE_HEIGHT; i++) {
      for (int j = 0; j < StaticData.TABLE_WIDTH; j++) {
        this.theBigArray[i][j] = false;
        this.theBigColorArr[i][j] = -1;
      }
    }
    // END GENERATE THE BIG EMPTY ARRAYS
  }

  private void recalculateStickFigure(int startX, int xPosRaw,
      LeftSide leftSideInstance) {

    // THIS CODE IS ONLY FOR STICK FIGURE !!!
    // THIS CODE IS ONLY FOR STICK FIGURE !!!
    if ((xPosRaw + this.leftEmptyColumns) == 1) {
      if (this.leftEmptyColumns == 2) {
        leftSideInstance.setStartX(startX
            + StaticData.GET_SQUARE_SIZE());
        for (int i = 0; i < 4; i++) {
          for (int j = 0; j < 4; j++) {
            this.figureArr[i][j] = j == 1;
          }
        }

      }

    } else if ((xPosRaw + 4 - this.rightEmptyColumns) == StaticData.TABLE_WIDTH - 1) {
      if (this.rightEmptyColumns == 2) {
        leftSideInstance.setStartX(startX
            - StaticData.GET_SQUARE_SIZE());
        for (int i = 0; i < 4; i++) {
          for (int j = 0; j < 4; j++) {
            this.figureArr[i][j] = j == 2;
          }
        }
      }
    }
    this.upAndDownEmptyCounter(this.figureArr, true);
    this.leftAndRightEmptyCounter(this.figureArr, true);

    // THIS CODE IS ONLY FOR STICK FIGURE !!!
    // THIS CODE IS ONLY FOR STICK FIGURE !!!

  }

  public boolean rotateBorder(int startY, int startX,
      LeftSide leftSideInstance) {

    int yPos = startY / StaticData.GET_SQUARE_SIZE();
    int xPosRaw = startX / StaticData.GET_SQUARE_SIZE();

    this.upAndDownEmptyCounter(this.figureArr, true);
    this.leftAndRightEmptyCounter(this.figureArr, true);

    if ((this.figyreType == 0)
        && ((xPosRaw + this.leftEmptyColumns) == 1 || (xPosRaw + 4 - this.rightEmptyColumns) == StaticData.TABLE_WIDTH - 1)) {
      this.recalculateStickFigure(startX, xPosRaw, leftSideInstance);
    }

    boolean[][] testFigure = this.rotateTempFigure(this.figureArr);

    this.upAndDownEmptyCounter(testFigure, false);
    this.leftAndRightEmptyCounter(testFigure, false);

    int arrayWidthRot = 4 - this.leftEmptyColRotate
        - this.rightEmptyColRotate;
    int arrayHeightRot = 4 - this.downEmptyRowsRotate;

    // X START POINT
    int xPosRot = xPosRaw + this.leftEmptyColRotate;
    // X END POINT
    int xEndPointRot = xPosRot + arrayWidthRot;
    // Y START POINT yPos
    // yPos
    // Y END POINT
    int yEndPointRot = yPos + arrayHeightRot;

    boolean status = (xPosRot < 0 || xEndPointRot > StaticData.TABLE_WIDTH)
        || (yEndPointRot > StaticData.TABLE_HEIGHT);

    if (this.figyreType == 6) {
      status = true;
    }

    return status;
  }

  public boolean rotateByBigArr(int startY, int startX) {

    int yPos = startY / StaticData.GET_SQUARE_SIZE();
    int xPosRaw = startX / StaticData.GET_SQUARE_SIZE();

    boolean[][] testFigure = this.rotateTempFigure(this.figureArr);
    // // TODO

    this.upAndDownEmptyCounter(testFigure, false);
    this.leftAndRightEmptyCounter(testFigure, false);

    int arrayWidthRot = 4 - this.leftEmptyColRotate
        - this.rightEmptyColRotate;
    int arrayHeightRot = 4 - this.downEmptyRowsRotate;

    // X START POINT
    int xPosRot = xPosRaw + this.leftEmptyColRotate;
    // X END POINT
    int xEndPointRot = xPosRot + arrayWidthRot;
    // Y START POINT yPos
    // yPos
    // Y END POINT

    int yEndPointRot = yPos + arrayHeightRot;

    boolean status = false;
    for (int i = 0; i < StaticData.TABLE_HEIGHT; i++) {
      for (int j = 0; j < StaticData.TABLE_WIDTH; j++) {

        if ((j >= xPosRot && j < xEndPointRot)
            && (i >= yPos && i < yEndPointRot)) {
          if (testFigure[i - yPos][j + this.leftEmptyColRotate
              - xPosRot]) {
            if (this.theBigArray[i][j]) {
              status = true;
            }

          }
        }
      }

    }

    return status;

  }

  public boolean[][] rotateFigure() {
    this.figureArr = this.rotateTempFigure(this.figureArr);

    for (int i = 0; i < figureArr.length; i++) {
      for (int j = 0; j < figureArr.length; j++) {
        System.out.print("\t" + figureArr[i][j]);
      }
      System.out.println();
    }
    System.out.println("=============================");
    this.upAndDownEmptyCounter(this.figureArr, true);
    this.leftAndRightEmptyCounter(this.figureArr, true);

    return this.figureArr;
  }

  private boolean[][] rotateTempFigure(boolean[][] mat) {

    int w = mat.length;
    int h = mat[0].length;
    boolean[][] ret = new boolean[h][w];
    for (int i = 0; i < h; ++i) {
      for (int j = 0; j < w; ++j) {
        ret[i][j] = mat[w - j - 1][i];
      }
    }
    return ret;
  }

  // System.out.print(" MT--> " + figureArr[i][1]); FOR TYPE 0 -> TRUE*4
  public boolean moveDown(int startY, int startX) {
    int yPos = startY / StaticData.GET_SQUARE_SIZE();
    int xPos = startX / StaticData.GET_SQUARE_SIZE();

    boolean status = false;
    // this.printMatrix();
    for (int j = 0; j < 4; j++) {
      for (int i = 0; i < 4; i++) {
        if (i + 1 < 4) {
          if ((!figureArr[i + 1][j] && figureArr[i][j])) {
            // SEARH FOR THE BIG ARR ELEMENT
            if (yPos > 0) {
              if ((yPos + i + 1) < StaticData.TABLE_HEIGHT) {
                if (theBigArray[yPos + i + 1][j + xPos]) {
                  this.updateTheBigArray(xPos, yPos);
                  status = true;
                }
              }

            }

          }
        } else if (figureArr[3][j]) {
          if (yPos + i + 1 < StaticData.TABLE_HEIGHT) {
            if (theBigArray[yPos + i + 1][j + xPos]) {
              this.updateTheBigArray(xPos, yPos);
              status = true;
            }
          }
        }
      }

    }
    return status;
  }

  public boolean toOneSide(int startY, int startX, int stepX) {
    int yPos = startY / StaticData.GET_SQUARE_SIZE();
    int xPos = startX / StaticData.GET_SQUARE_SIZE();

    boolean status = false;
    if (yPos > 0) {
      if (stepX > 0) {
        for (int i = 0; i < 4; i++) {
          for (int j = 0; j < 4; j++) {

            if (j + 1 < 4) {
              if (!this.figureArr[i][j + 1]
                  && this.figureArr[i][j]) {
                if ((xPos + j + 1) < StaticData.TABLE_WIDTH) {
                  if (this.theBigArray[yPos + i][xPos + j + 1]) {
                    status = true;
                  }
                }

              }
            } else if (this.figureArr[i][3]) {
              if ((xPos + j + 1) < StaticData.TABLE_WIDTH) {
                if (this.theBigArray[yPos + i][xPos + j + 1]) {
                  status = true;
                }
              }

            }

          }
        }

      } else {
        for (int i = 3; i >= 0; i--) {
          for (int j = 3; j >= 0; j--) {

            if (j - 1 >= 0) {
              if (!this.figureArr[i][j - 1]
                  && this.figureArr[i][j]) {
                if ((xPos + j - 1) >= 0) {
                  if (this.theBigArray[yPos + i][xPos + j - 1]) {
                    status = true;
                  }
                }

              }
            } else if (this.figureArr[i][0]) {
              if ((xPos + j - 1) >= 0) {
                if (this.theBigArray[yPos + i][xPos + j - 1]) {
                  status = true;
                }
              }
            }
          }
        }
      }

    }

    return status;
  }

  public boolean toOneSideBorder(int stepX, int startX) {
    this.leftAndRightEmptyCounter(this.figureArr, true);
    boolean status = true;

    // MOVE LEFT
    if (stepX < 0) {
      if (startX > -this.leftEmptyColumns
          * StaticData.GET_SQUARE_SIZE()) {
        status = false;
      }

    }
    // MOVE RIGHT
    if (stepX > 0) {
      if (startX < StaticData.TABLE_HORIZONTAL_SQUARE_COUNT
          - (4 - this.rightEmptyColumns)
          * StaticData.GET_SQUARE_SIZE()) {
        status = false;
      }
    }

    return status;
  }

  public boolean moveDownBorder(int startX, int startY) {
    int yPos = startY / StaticData.GET_SQUARE_SIZE();
    int xPos = startX / StaticData.GET_SQUARE_SIZE();
    this.upAndDownEmptyCounter(this.figureArr, true);
    boolean status = false;

    int positionPlusFigureHeight = startY + (4 - this.downEmptyRows)
        * StaticData.GET_SQUARE_SIZE();

    if (positionPlusFigureHeight >= StaticData.TABLE_VERTICAL_SQUARE_COUNT) {
      status = true;
      this.updateTheBigArray(xPos, yPos);

    }

    return status;
  }

  private void updateTheBigArray(int xPosRaw, int yPos) {

    this.leftAndRightEmptyCounter(this.figureArr, true);

    int arrayWidth = 4 - leftEmptyColumns - rightEmptyColumns;
    int arrayHeight = 4 - downEmptyRows;

    // X START POINT
    int xPos = xPosRaw + leftEmptyColumns;
    // X END POINT
    int xEndPoint = xPos + arrayWidth;
    // Y START POINT yPos
    // yPos
    // Y END POINT
    int yEndPoint = yPos + arrayHeight;

    for (int i = 0; i < StaticData.TABLE_HEIGHT; i++) {
      for (int j = 0; j < StaticData.TABLE_WIDTH; j++) {
        if (j == xPos && i == yPos) {

        }
        if ((j >= xPos && j < xEndPoint)
            && (i >= yPos && i < yEndPoint)) {
          if (this.figureArr[i - yPos][j + leftEmptyColumns - xPos]) {
            this.theBigArray[i][j] = true;
            this.theBigColorArr[i][j] = this.figyreType;
          } else {
            this.theBigArray[i][j] = this.theBigArray[i][j];
            this.theBigColorArr[i][j] = this.theBigColorArr[i][j];
          }

        } else {
          this.theBigArray[i][j] = this.theBigArray[i][j];
          this.theBigColorArr[i][j] = this.theBigColorArr[i][j];
        }

      }

    }

  }

  // GET HORISONTAL SIZES GET HORISONTAL SIZES GET HORISONTAL SIZES
  // GET HORISONTAL SIZES GET HORISONTAL SIZES GET HORISONTAL SIZES

  private void leftAndRightEmptyCounter(boolean[][] figure, boolean isReal) {

    // IF COLUMN CONTAIN TRUE VALUE, THE COLUMN IS TRUE
    boolean[] emptyColumnLeftToRight = new boolean[4];

    for (int i = 0; i < 4; i++) {
      boolean isNotEmptyColumn = false;
      for (int j = 0; j < 4; j++) {
        // System.out.println(currentlyGeneratedFigure[j][i]);
        if (figure[j][i]) {
          isNotEmptyColumn = true;
        }
        emptyColumnLeftToRight[i] = isNotEmptyColumn;
      }
    }

    boolean flagIsNotEmpty = false;

    int leftEmptyCounter = 0;
    int rightEmptyCounter = 0;

    // GET UP EMPTY ROWS
    for (boolean boolVal : emptyColumnLeftToRight) {
      // System.out.println("dsdsd-->" + boolVal);
      if (boolVal) {
        flagIsNotEmpty = true;
      }

      if (!flagIsNotEmpty && !boolVal) {
        leftEmptyCounter++;
      }

      if (flagIsNotEmpty && !boolVal) {
        rightEmptyCounter++;
      }

    }

    if (isReal) {
      this.leftEmptyColumns = leftEmptyCounter;
      this.rightEmptyColumns = rightEmptyCounter;
    } else {
      this.leftEmptyColRotate = leftEmptyCounter;
      this.rightEmptyColRotate = rightEmptyCounter;
    }

  }

  // GET HORISONTAL SIZES GET HORISONTAL SIZES GET HORISONTAL SIZES
  // GET HORISONTAL SIZES GET HORISONTAL SIZES GET HORISONTAL SIZES

  // GET VERTICAL SIZES GET VERTICAL SIZES GET VERTICAL SIZES
  // GET VERTICAL SIZES GET VERTICAL SIZES GET VERTICAL SIZES

  private void upAndDownEmptyCounter(boolean[][] figure, boolean isReal) {

    // IF ROW CONTAIN TRUE VALUE, THE ROW IS TRUE
    boolean[] emptyRowsUpToDown = new boolean[4];

    for (int i = 0; i < 4; i++) {
      boolean isNotEmptyRow = false;
      for (int j = 0; j < 4; j++) {
        // System.out.println(currentlyGeneratedFigure[j][i]);
        if (figure[i][j]) {
          isNotEmptyRow = true;
        }
        emptyRowsUpToDown[i] = isNotEmptyRow;
      }
    }

    boolean flagIsNotEmpty = false;

    int upEmptyCounter = 0;
    int downEmptyCounter = 0;

    // GET UP EMPTY ROWS
    for (boolean boolVal : emptyRowsUpToDown) {
      // System.out.println("dsdsd-->" + boolVal);
      if (boolVal) {
        flagIsNotEmpty = true;
      }

      if (!flagIsNotEmpty && !boolVal) {
        upEmptyCounter++;
      }

      if (flagIsNotEmpty && !boolVal) {
        // upEmptyCounter++;
        downEmptyCounter++;
      }

    }

    if (isReal) {
      this.downEmptyRows = downEmptyCounter;
      this.upEmptyRows = upEmptyCounter;
    } else {
      this.downEmptyRowsRotate = downEmptyCounter;
      this.upEmptyRowsRotate = upEmptyCounter;
    }

  }

  // GET VERTICAL SIZES GET VERTICAL SIZES GET VERTICAL SIZES
  // GET VERTICAL SIZES GET VERTICAL SIZES GET VERTICAL SIZES

  public void checkForNewPoints(boolean callFirst) {
    boolean[] filledRows = new boolean[StaticData.TABLE_HEIGHT];

    for (int i = StaticData.TABLE_HEIGHT - 1; i >= 0; i--) {
      boolean isFilled = true;
      for (int j = 0; j < StaticData.TABLE_WIDTH; j++) {
        if (!this.theBigArray[i][j]) {
          isFilled = false;
          break;
        }
      }
      filledRows[i] = isFilled;
    }

//    TODO The logic for the removing of the filled rows has to be improved!
    int sumFilledRows = 0;
    for (boolean filledRow : filledRows) {
      if (filledRow) {
        sumFilledRows++;
      }
    }

    if (sumFilledRows > 0) {
      this.removefilledRows(filledRows, sumFilledRows);
      if (callFirst) {
        this.points = this.points + sumFilledRows * sumFilledRows;
      }
    }

  }

  private void removefilledRows(boolean[] allRows, int sumFilledRows) {

    boolean[][] newBigArr = new boolean[StaticData.TABLE_HEIGHT][StaticData.TABLE_WIDTH];

    int[][] newBigColorArr = new int[StaticData.TABLE_HEIGHT][StaticData.TABLE_WIDTH];

    for (int i = 0; i < StaticData.TABLE_HEIGHT; i++) {
      System.out.println("KEY IS: " + i + " AND VALUE IS: " + allRows[i]);
    }

    for (int j2 = 0; j2 < StaticData.TABLE_HEIGHT; j2++) {
      if (allRows[j2]) {
        for (int i = 0; i < j2; i++) {
          for (int j = 0; j < StaticData.TABLE_WIDTH; j++) {
            newBigArr[i + 1][j] = this.theBigArray[i][j];
            newBigColorArr[i + 1][j] = this.theBigColorArr[i][j];
          }
        }
      } else {
        for (int k = 0; k < StaticData.TABLE_WIDTH; k++) {
          newBigArr[j2][k] = this.theBigArray[j2][k];
          newBigColorArr[j2][k] = this.theBigColorArr[j2][k];
        }
      }
    }

    this.theBigArray = newBigArr;
    this.theBigColorArr = newBigColorArr;

    for (int i = 0; i < sumFilledRows - 1; i++) {
      this.checkForNewPoints(false);
    }
  }

}
