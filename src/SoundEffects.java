import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

public class SoundEffects implements Runnable {

	private Thread soundThread;

	private String soundPath;

	public Thread getSoundThread() {
		return soundThread;
	}

	public void playHit(int frequency) {

		if (frequency == StaticData.MOVE_DOWN_NORMAL) {
			this.soundPath = StaticData.HIT_NORMAL;
		} else {
			this.soundPath = StaticData.HIT_SPEED;
		}

		this.soundThread = new Thread(this);
		this.soundThread.start();

	}

	@Override
	public void run() {

		InputStream fileStream = getClass().getResourceAsStream(this.soundPath);

		Player play = null;

		try {
			play = new Player(fileStream);
			play.play();
		} catch (JavaLayerException e) {
			e.printStackTrace();
		}

	}

}
