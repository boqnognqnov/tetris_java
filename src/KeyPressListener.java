import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyPressListener implements KeyListener {

	private LeftSide gamePanel = null;
	private Start start = null;

	public KeyPressListener(LeftSide gamePanel, Start start) {
		this.gamePanel = gamePanel;
		this.start = start;
	}

	public void keyTyped(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			start.setFrequency(StaticData.MOVE_DOWN_NORMAL);
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_LEFT) {

			gamePanel.setStepX(-StaticData.GET_SQUARE_SIZE());
			gamePanel.moveSide();
			gamePanel.repaint();

		} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

			gamePanel.setStepX(StaticData.GET_SQUARE_SIZE());
			gamePanel.moveSide();
			gamePanel.repaint();
		} else if (e.getKeyCode() == KeyEvent.VK_UP) {
			gamePanel.rotateFigure();
			gamePanel.repaint();

		} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			start.setFrequency(StaticData.MOVE_DOWN_FAST);

		} else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			start.changeStartresumeButtonValue();

		} else if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			// frame.dispose();
		}
	}
}
