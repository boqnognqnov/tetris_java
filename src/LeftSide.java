import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class LeftSide extends JPanel {
	private static final long serialVersionUID = 4149814524351242031L;
	private boolean[][] figureArr = null;

	private int figureType;

	private Image gamePanelImage;

	private boolean blockFigureMovement = false;

	private Start start;

	private ArraysCollaboration arrCollaboration;

	private int points = 0;

	public void setBlockFigureMovement(boolean blockFigureMovement) {
		this.blockFigureMovement = blockFigureMovement;
	}

	public void setFigureType(int figureType) {
		this.figureType = figureType;
	}

	public void setFigureArr(boolean[][] figureArr) {
		this.figureArr = figureArr;
	}

	public int getPoints() {
		return points;
	}

	public void setPiecetype(FigureGenerator piecetype) {
		this.figureArr = piecetype.copy();
		this.figureType = piecetype.getRandInt();
		this.arrCollaboration.setFigyreType(this.figureType);
		this.arrCollaboration.setFigureArr(this.figureArr);

	}

	private int startX = StaticData.FIRST_INIT_START_X;
	private int startY = StaticData.FIRST_INIT_START_Y;

	public void setStartX(int startX) {
		this.startX = startX;
	}

	public void setStartY(int startY) {
		this.startY = startY;
	}

	private int stepX = 0;

	// CREATE NEW FIGURE
	private boolean figureIsDown = false;

	public void setFigureIsDown(boolean figureIsDown) {
		this.figureIsDown = figureIsDown;
	}

	public boolean getFigureIsDown() {
		return figureIsDown;
	}

	public LeftSide(FigureGenerator piecetype, Start start) {
		this.start = start;

		// FIRST INIT THE BIG PROCESSING CLASS
		this.arrCollaboration = new ArraysCollaboration();

		BufferedInputStream stream = null;
		try {
			int[] frameSize = StaticData.GET_FRAME_SIZE();
			stream = (BufferedInputStream) getClass().getClassLoader()
					.getResourceAsStream(StaticData.PATH_LEFT_SIDE_BACKGROUND);
			BufferedImage bg = ImageIO.read(stream);
			stream.close();
			gamePanelImage = bg.getScaledInstance(frameSize[0], frameSize[1], Image.SCALE_SMOOTH);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		this.figureArr = piecetype.copy();
		this.figureType = piecetype.getRandInt();
		this.arrCollaboration.setFigyreType(this.figureType);
		this.arrCollaboration.setFigureArr(this.figureArr);

	}

	// PRINT ALL ARRAYS ON LEFT PANEL
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawImage(gamePanelImage, 0, 0, this);

		g.drawRect(0, 0, StaticData.TABLE_HORIZONTAL_SQUARE_COUNT, StaticData.TABLE_VERTICAL_SQUARE_COUNT);

		this.printTheBigArray(g);

		this.printFigure(g);
		this.printGameOver(g);
	}

	private void printGameOver(Graphics g) {
		if (start.isGameOver()) {
			InputStream is = getClass().getResourceAsStream(StaticData.PATH_MESSAGE_GAME_OVER_FONT);
			Font customFont = null;
			try {
				customFont = Font.createFont(Font.TRUETYPE_FONT, is);
			} catch (Throwable e) {
				e.printStackTrace();
			} finally {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			customFont = customFont.deriveFont(100F);

			g.setFont(customFont);
			g.setColor(Color.WHITE);
			g.drawString("Game Over", 70, 200);
			g.drawString("Game Over", 70, 260);
			g.drawString("Game Over", 70, 320);

		}
	}

	private void printFigure(Graphics g) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (this.figureArr[i][j] == true) {
					int xx = startX + j * StaticData.GET_SQUARE_SIZE();
					int yy = startY + i * StaticData.GET_SQUARE_SIZE();
					g.setColor(Color.decode(StaticData.colors[this.figureType]));
					g.fill3DRect(xx, yy, StaticData.GET_SQUARE_SIZE(), StaticData.GET_SQUARE_SIZE(), true);
				}

			}
		}
	}

	private void printTheBigArray(Graphics g) {
		int[][] theBigArray = arrCollaboration.getTheBigColorArray();
		for (int i = 0; i < StaticData.TABLE_HEIGHT; i++) {
			for (int j = 0; j < StaticData.TABLE_WIDTH; j++) {
				if (theBigArray[i][j] != -1) {
					int x = j * StaticData.GET_SQUARE_SIZE();
					int y = i * StaticData.GET_SQUARE_SIZE();
					g.setColor(Color.decode(StaticData.colors[theBigArray[i][j]]));
					g.fill3DRect(x, y, StaticData.GET_SQUARE_SIZE(), StaticData.GET_SQUARE_SIZE(), true);

				}

			}
		}

	}

	// HORIZONTALLY MOVEMENT CONDITIONS
	public void moveSide() {

		boolean oneSideBorder = arrCollaboration.toOneSideBorder(this.stepX, this.startX);
		boolean oneSideBigArray = arrCollaboration.toOneSide(this.startY, this.startX, this.stepX);

		if (oneSideBigArray == false && oneSideBorder == false && this.blockFigureMovement == false) {
			startX += stepX;
		}
//		else {
//			System.out.println("Something stand on a way !!!");
//		}

	}

	public void rotateFigure() {
		boolean border = arrCollaboration.rotateBorder(this.startY, this.startX, this);
		boolean rotateByBigArr = arrCollaboration.rotateByBigArr(this.startY, this.startX);

		if (border == false && rotateByBigArr == false && this.blockFigureMovement == false) {
			this.figureArr = arrCollaboration.rotateFigure();
		}
	}

	public void moveDown() {

		boolean border = arrCollaboration.moveDownBorder(this.startX, this.startY);
		boolean bigArray = arrCollaboration.moveDown(this.startY, this.startX);
		if (border == true || bigArray == true) {
			figureIsDown = true;
			this.checkForEndGame();
			start.playHitSound("hit");
			this.start.setFrequency(StaticData.MOVE_DOWN_NORMAL);
			this.startX = StaticData.FIRST_INIT_START_X;
			this.startY = StaticData.FIRST_INIT_START_Y;

			arrCollaboration.checkForNewPoints(true);
			this.points = arrCollaboration.getPoints();

			System.out.println("POINTS->> " + this.points);
			return;
		}
		this.startY += StaticData.GET_SQUARE_SIZE();

	}

	private void checkForEndGame() {
		if (this.startY == StaticData.FIRST_INIT_START_Y) {
			start.setGameOver(true);
			InfoAndButtonsPanel dsd = this.start.getControlsPanel();
			dsd.getButtonStartResume().setText("Start");
			dsd.repaint();

			this.start.suspendedFigureMovement();

		}
	}

	// KEY LISTENER SET STEP_X FROM CONFIG DATA
	public void setStepX(int stepX) {
		this.stepX = stepX;
	}

}
