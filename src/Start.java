import java.awt.*;
import javax.swing.*;

public class Start implements Runnable {
  private static JFrame frame = null;
  private RightSide rightSide = null;
  private LeftSide leftSide = null;
  private FigureGenerator currentFigure = null;
  private FigureGenerator nextFigure = null;
  private boolean suspended = false;
  private Thread moveDownThread;
  private int frequency = StaticData.MOVE_DOWN_NORMAL;
  private final SoundEffects soundClass;
  private boolean soundIsOff = true;

  private boolean isFirstStart = true;
  private boolean isGameOver = false;

  private InfoAndButtonsPanel controlsPanel;


  public static void main(String[] args) {
    new Start();
  }

  public InfoAndButtonsPanel getControlsPanel() {
    return controlsPanel;
  }

  public void setControlsPanel(InfoAndButtonsPanel controlsPanel) {
    this.controlsPanel = controlsPanel;
  }

  public boolean isGameOver() {
    return isGameOver;
  }

  public void setGameOver(boolean isGameOver) {
    this.isGameOver = isGameOver;
  }

  public void setSoundIsOff(boolean soundIsOff) {
    this.soundIsOff = soundIsOff;
  }

  public boolean isFirstStart() {
    return isFirstStart;
  }

  public void setFirstStart(boolean isFirstStart) {
    this.isFirstStart = isFirstStart;
  }

  public void setFrequency(int frequency) {
    this.frequency = frequency;
  }

  public Start() {
    frame = new JFrame("jTetris Beta");
    int[] size = StaticData.GET_FRAME_SIZE();
    frame.setSize(size[0], size[1]);
    frame.setResizable(false);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    this.newGame();
    frame.setVisible(true);

    frame.addKeyListener(new KeyPressListener(leftSide, this));
    frame.setFocusable(true);

    this.soundClass = new SoundEffects();

  }

  public void newGame() {
    frame.repaint();

    setCurrentAndNextFigure();

    this.isGameOver = false;
    this.suspended = false;

    // INIT LEFT PANEL
    leftSide = new LeftSide(currentFigure, this);

    // INIT MAIN GRID SYSTEM
    Dimension dim = new Dimension(StaticData.GET_SQUARE_SIZE() * StaticData.TABLE_WIDTH,
        StaticData.GET_SQUARE_SIZE() * StaticData.TABLE_HEIGHT);
    leftSide.setPreferredSize(dim);
    // INIT MAIN GRID SYSTEM
    // END // INIT LEFT PANEL

    // INIT RIGHT PANEL
    rightSide = new RightSide(nextFigure, frame, this);
    // END // INIT RIGHT PANEL

    // INCLUDE PANELS
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());

    panel.add(leftSide, BorderLayout.WEST);
    panel.add(rightSide, BorderLayout.CENTER);

    frame.add(panel);
    // END INCLUDE PANELS

    this.startThread();

  }

  public void suspendedFigureMovement() {
    leftSide.setBlockFigureMovement(true);
    this.suspended = true;

  }

  public boolean isSuspended() {
    return suspended;
  }

  public void startThread() {

    if (moveDownThread == null) {
      moveDownThread = new Thread(this);
      moveDownThread.start();
    }

  }

  public synchronized void resume() {
    suspended = false;
    leftSide.setBlockFigureMovement(false);
    notify();
  }

  @Override
  public void run() {
    try {
      while (!suspended) {
        if (this.isFirstStart) {
          this.suspendedFigureMovement();
        }
        System.err.println(StaticData.GET_SQUARE_SIZE());
        Thread.sleep(frequency);
        leftSide.moveDown();
        this.threadProccessing();

        frame.repaint();
        synchronized (this) {
          while (suspended) {
            wait();
          }

        }
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }

  public void threadProccessing() {
    if (leftSide.getFigureIsDown()) {

      this.setCurrentAndNextFigure();

      leftSide.setPiecetype(currentFigure);
      leftSide.setFigureIsDown(false);
      rightSide.setNextFigure(nextFigure);
      rightSide.drawNextFigire();
      rightSide.setLeftSideObj(leftSide);
    }
  }

  private void setCurrentAndNextFigure() {

    if (nextFigure == null) {
      nextFigure = new FigureGenerator();
    }
    currentFigure = nextFigure;

    nextFigure = new FigureGenerator();
  }

  public void changeStartresumeButtonValue() {
    JButton startResumeButton = controlsPanel.getButtonStartResume();

    if (startResumeButton.getText().equals("Start") && this.isFirstStart) {
      this.setFirstStart(false);
      this.resume();
      startResumeButton.setText("Pause");
    } else if (startResumeButton.getText().equals("Start") && this.isGameOver) {
      this.newGame();
      this.resume();
      startResumeButton.setText("Pause");
    } else if (startResumeButton.getText().equals("Pause")) {
      this.suspendedFigureMovement();
      startResumeButton.setText("Resume");
    } else if (startResumeButton.getText().equals("Resume")) {
      this.resume();
      startResumeButton.setText("Pause");
    }
    rightSide.repaint();
  }

  public void playHitSound(String action) {
    if (!this.soundIsOff) {
      if (action.equals("hit")) {
        this.soundClass.playHit(this.frequency);
      }
    }

  }

}
