import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import java.util.Objects;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class InfoAndButtonsPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private Image gamePanelImage;
	private JLabel pointsLevel;
	private LeftSide leftSide;
	private JButton buttonStartResume;
	private JButton buttonVolume;

	public JButton getButtonVolume() {
		return buttonVolume;
	}

	public JButton getButtonStartResume() {
		return buttonStartResume;
	}

	public InfoAndButtonsPanel(JFrame frame, Start start) {

		this.setLayout(new BorderLayout());

		Font customFont = null;

		try {
			InputStream is = getClass().getResourceAsStream(StaticData.PATH_MESSAGE_SCORE_FONT);
			customFont = Font.createFont(Font.TRUETYPE_FONT, is);
			customFont = customFont.deriveFont(34F);

		} catch (FontFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			customFont = new Font("pointsFont", Font.BOLD, 34);
			e1.printStackTrace();
		}

		pointsLevel = new JLabel(0 + " Points");
		pointsLevel.setFont(customFont);
		pointsLevel.setHorizontalAlignment(SwingConstants.CENTER);
		pointsLevel.setForeground(Color.WHITE);
		this.add(pointsLevel, BorderLayout.NORTH);

		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setLayout(null);
		buttonsPanel.setOpaque(false);


		ImageIcon buttonsImage = new ImageIcon(Objects.requireNonNull(getClass().getResource(StaticData.PATH_BUTTONS_BACKGROUND)));

		buttonStartResume = new JButton("Start", buttonsImage);
		buttonStartResume.setHorizontalTextPosition(JButton.CENTER);
		buttonStartResume.setVerticalTextPosition(JButton.CENTER);
		buttonStartResume.setFont(customFont);
		buttonStartResume.setForeground(Color.WHITE);
		buttonStartResume.setBounds(20, 20, 150, 50);
		buttonStartResume.addActionListener(new KeyStartPauseButton(frame, start, this));

		buttonsPanel.add(buttonStartResume);

		JButton buttonAbout = new JButton("About", buttonsImage);
		buttonAbout.setHorizontalTextPosition(JButton.CENTER);
		buttonAbout.setVerticalTextPosition(JButton.CENTER);
		buttonAbout.setFont(customFont);
		buttonAbout.setForeground(Color.WHITE);
		buttonAbout.setBounds(20, 90, 150, 50);
		buttonAbout.addActionListener(new KeyAboutButtonEvent(frame));

		buttonsPanel.add(buttonAbout);

		buttonVolume = new JButton("Vol On", buttonsImage);
		buttonVolume.setHorizontalTextPosition(JButton.CENTER);
		buttonVolume.setVerticalTextPosition(JButton.CENTER);
		buttonVolume.setFont(customFont);
		buttonVolume.setForeground(Color.WHITE);
		buttonVolume.setBounds(20, 160, 150, 50);
		buttonVolume.addActionListener(new KeyVolumeButtonEvent(frame, start, this));

		buttonsPanel.add(buttonVolume);

		this.add(buttonsPanel, BorderLayout.CENTER);

		JLabel createdBy = new JLabel();
		createdBy.setText("Created by Boyan Bozhidarov");
		createdBy.setForeground(Color.BLUE);
		createdBy.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(createdBy, BorderLayout.SOUTH);

		try {
			int[] frameSize = StaticData.GET_FRAME_SIZE();
			BufferedImage bg = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(StaticData.PATH_BUTTONS_AREA_BACKGROUND)));
			gamePanelImage = bg.getScaledInstance(frameSize[0], frameSize[1], Image.SCALE_SMOOTH);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		start.setControlsPanel(this);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(gamePanelImage, -100, 0, this);

	}

	public void setLeftSideobj(LeftSide leftSide) {
		this.leftSide = leftSide;

	}

	public void refreshInfo() {
		this.pointsLevel.setText(Integer.toString(leftSide.getPoints()) + " Points");

	}

}
