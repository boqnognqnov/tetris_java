public class StaticData {
	// public static final int SQUARE_SIZE = 50;
	public static final int TABLE_WIDTH = 10;
	public static final int TABLE_HEIGHT = 18;

	public static final int MOVE_DOWN_FAST = 50;
	public static final int MOVE_DOWN_NORMAL = 300;

//	PATHS
	public static final String HIT_SPEED = "resources/sounds/speedHit.mp3";
	public static final String HIT_NORMAL = "resources/sounds/normalHit.mp3";
	
//	FONTS
	public static final String PATH_MESSAGE_GAME_OVER_FONT = "resources/fonts/BALLSONTHERAMPAGE.ttf";
	public static final String PATH_MESSAGE_SCORE_FONT = "resources/fonts/BALLSONTHERAMPAGE.ttf";
	
//	LAYOUT BACKGROUNDS
	public static final String PATH_LEFT_SIDE_BACKGROUND = "resources/images/4.jpg";
	public static final String PATH_NEXT_PIECE_BACKGROUND = "resources/images/7.jpg";
	public static final String PATH_BUTTONS_AREA_BACKGROUND = "resources/images/7.jpg";

	public static final String PATH_BUTTONS_BACKGROUND = "resources/images/buttons6.jpg";
	
	
	

	
	
	

	// FIGURE COLOR
	public final static String colors[] = { "#F5003D", "#3DF500", "#003DF5", "#B800F5", "#F5B800", "#6699CC",
			"#8A5C2E" };

	// END FIGURE COLOR

	public static int GET_SQUARE_SIZE() {
		int squareSize = 0;
		ResponsiveParams rp = new ResponsiveParams();
		int[] widthHeightArr = rp.getCurrentResolution();

		squareSize = (widthHeightArr[1] / StaticData.TABLE_HEIGHT) - 4;
		return squareSize;
	}

	public static int[] GET_FRAME_SIZE() {
		int[] size = new int[2];

		ResponsiveParams rp = new ResponsiveParams();
		int[] widthHeightArr = rp.getCurrentResolution();

		if (widthHeightArr[0] == 1280) {
			size[0] = (int) (widthHeightArr[0] * 0.65);
		} else if (widthHeightArr[0] > 1024) {
			size[0] = (int) (widthHeightArr[0] * 0.55);
		} else if (widthHeightArr[0] == 1024) {
			size[0] = (int) (widthHeightArr[0] * 0.65);
		} else {
			size[0] = (int) (widthHeightArr[0] * 0.7);
		}

		if (StaticData.TABLE_WIDTH < 12) {
			size[0] = (int) (size[0] * 0.9);
		}

		size[1] = widthHeightArr[1] - 52;

		return size;
	}

	public static final int TABLE_VERTICAL_SQUARE_COUNT = TABLE_HEIGHT * StaticData.GET_SQUARE_SIZE();

	public static final int TABLE_HORIZONTAL_SQUARE_COUNT = TABLE_WIDTH * StaticData.GET_SQUARE_SIZE();

	public static final int FIRST_INIT_START_Y = -StaticData.GET_SQUARE_SIZE() * 3;
	public static final int FIRST_INIT_START_X = StaticData.GET_SQUARE_SIZE() * 3;

	public final static boolean FIGURES_ARRAY[][][] = {
			{ { false, true, false, false }, { false, true, false, false }, { false, true, false, false },
					{ false, true, false, false }, },
			{ { false, false, false, false }, { false, true, true, false }, { false, true, false, false },
					{ false, true, false, false }, },
			{ { false, false, false, false }, { false, true, false, false }, { false, true, false, false },
					{ false, true, true, false }, },
			{ { false, false, false, false }, { false, true, false, false }, { false, true, true, false },
					{ false, false, true, false }, },
			{ { false, false, false, false }, { false, false, true, false }, { false, true, true, false },
					{ false, true, false, false }, },
			{ { false, false, false, false }, { false, true, false, false }, { false, true, true, false },
					{ false, true, false, false }, },
			{ { false, false, false, false }, { false, false, false, false }, { false, true, true, false },
					{ false, true, true, false }, }, };

	// DUMP FIGURES
	public static void printAllFigures() {
		for (boolean[][] bs : FIGURES_ARRAY) {
			System.out.println("======================");
			// System.out.println("1");
			for (boolean[] bs2 : bs) {

				for (boolean bs3 : bs2) {
					if (bs3 == false) {
						System.out.print(Boolean.toString(bs3));

					} else {
						System.out.print("#####");

					}

				}

				System.out.println();
			}
		}

	}
}
