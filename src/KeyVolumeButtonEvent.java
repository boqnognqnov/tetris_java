import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

public class KeyVolumeButtonEvent implements ActionListener {

	private JFrame frame;
	private Start start;
	private InfoAndButtonsPanel controls;

	public KeyVolumeButtonEvent(JFrame frame, Start start, InfoAndButtonsPanel controls) {
		this.frame = frame;
		this.start = start;
		this.controls = controls;

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.frame.requestFocusInWindow();

		String buttonValue = this.controls.getButtonVolume().getText();
		if (buttonValue.equals("Vol On")) {
			this.controls.getButtonVolume().setText("Vol Off");
			this.start.setSoundIsOff(false);
		} else {
			this.controls.getButtonVolume().setText("Vol On");
			this.start.setSoundIsOff(true);
		}
		this.controls.repaint();
	}

}
