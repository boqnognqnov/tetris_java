import java.util.Random;

public class FigureGenerator {

	private int randInt;

	public int getRandInt() {
		return randInt;
	}

	private boolean[][] currentlyGeneratedFigure;

	public FigureGenerator() {
		// CONSTRUCTOR
		newRandomType();
	}

	public void newRandomType() {

		Random random = new Random();
		randInt = random.nextInt(7);// 7 cases
		// VERTICAL FIGURE TYPE
//		 randInt = 0;

	}

	public boolean[][] copy() {
		currentlyGeneratedFigure = new boolean[4][4];
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				currentlyGeneratedFigure[i][j] = StaticData.FIGURES_ARRAY[randInt][i][j];
			}
		}
		// this.setGlobalsVar();
		this.printMatrix();
		// StaticData.printAllFigures();

		return currentlyGeneratedFigure;
	}

	// DUMP PRINT MATRIX FIGURE
	private void printMatrix() {
		// DUMP PRINT MATRIX FIGURE
		System.out.println();
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print(" MT--> " + currentlyGeneratedFigure[i][j]);
			}
			System.out.println();
		}
	}
	// DUMP PRINT MATRIX FIGURE
}
